package com.kohls.bd.testunify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicates;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@Configuration
public class TestUnifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestUnifyApplication.class, args);
	}
	
	 @Bean
	    public Docket productApi() {
	        return new Docket(DocumentationType.SWAGGER_2)
	                .useDefaultResponseMessages(false)
	                .apiInfo(apiInfo())
	                .select()
	                .paths(Predicates.not(PathSelectors.regex("/error.*")))
	                .build();
	    }
	 
	    private ApiInfo apiInfo() {
	    	ApiInfo apiInfo = new ApiInfo("Lineage API", "API to get Azkaban flow lineage", "0.1",
	    			"NA", new Contact("NA", "NA", "NA"), "NA", "NA");
	        return apiInfo;
	    }
}

