package com.kohls.bd.testunify.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kohls.bd.testunify.model.TestConfig;
import com.kohls.bd.testunify.model.TestOutput;
import com.kohls.bd.testunify.service.TestUnifyService;

@RestController
@RequestMapping("/unify")
public class UnifyController {

	private final Logger logger = LoggerFactory.getLogger(UnifyController.class);

	@Autowired
	private TestUnifyService testUnifyService;

	@RequestMapping(value = "/execute", method = RequestMethod.POST)
	public List<TestOutput> execute(@RequestBody List<TestConfig> testConfig) {
		return testUnifyService.executeTests(testConfig);

	}

}
