package com.kohls.bd.testunify.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kohls.bd.testunify.model.ClusterConfig;
import com.kohls.bd.testunify.model.TestCase;
import com.kohls.bd.testunify.service.ClusterConfigRepository;
import com.kohls.bd.testunify.service.TestRepository;

@RestController
@RequestMapping("/config")
public class ConfigController {

	private final Logger logger = LoggerFactory.getLogger(ConfigController.class);

	@Autowired
	private ClusterConfigRepository clusterConfigRepository;

	@Autowired
	private TestRepository testRepository;

	@RequestMapping(value = "/cluster-config", method = RequestMethod.POST)
	public void execute(@RequestParam String env, @RequestBody ClusterConfig clusterConfig) { 
		clusterConfigRepository.setConfig(env, clusterConfig);
	}
	
	@RequestMapping(value = "/cluster-config", method = RequestMethod.GET)
	public Map<String, ClusterConfig> execute() { 
		//for (ClusterConfig  clusterConfig2: clusterConfig){
		return clusterConfigRepository.getAllConfig();
		
		//}
	}

	@RequestMapping(value = "/test-case", method = RequestMethod.POST)
	public void execute(@RequestParam String name, @RequestBody TestCase testCase) {
		testRepository.setTestCase(name, testCase);

	}
}