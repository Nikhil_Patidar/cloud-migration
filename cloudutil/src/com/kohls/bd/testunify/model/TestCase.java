package com.kohls.bd.testunify.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TestCase {

	private String name;
	private String projectName;
	private String flowName;
	private List<String> reconJobs;
	private List<String> skipJobsWithDataCopy;
	private List<String> skipJobs = null;
	private List<String> checkInput = null;
	private List<InputLocation> inputLocation = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFlowName() {
		return flowName;
	}

	public void setFlowName(String flowName) {
		this.flowName = flowName;
	}

	public List<String> getReconJobs() {
		return reconJobs;
	}

	public void setReconJobs(List<String> reconJobs) {
		this.reconJobs = reconJobs;
	}

	public List<String> getSkipJobsWithDataCopy() {
		return skipJobsWithDataCopy;
	}

	public void setSkipJobsWithDataCopy(List<String> skipJobsWithDataCopy) {
		this.skipJobsWithDataCopy = skipJobsWithDataCopy;
	}

	public List<String> getSkipJobs() {
		return skipJobs;
	}

	public void setSkipJobs(List<String> skipJobs) {
		this.skipJobs = skipJobs;
	}

	public List<String> getCheckInput() {
		return checkInput;
	}

	public void setCheckInput(List<String> checkInput) {
		this.checkInput = checkInput;
	}

	public List<InputLocation> getInputLocation() {
		return inputLocation;
	}

	public void setInputLocation(List<InputLocation> inputLocation) {
		this.inputLocation = inputLocation;
	}

}