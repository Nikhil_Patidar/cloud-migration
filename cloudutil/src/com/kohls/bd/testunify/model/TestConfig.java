package com.kohls.bd.testunify.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TestConfig {

	private String testCase;
	private List<String> envs = null;
	private String driverEnv;

	public String getTestCase() {
		return testCase;
	}

	public void setTestCase(String testCase) {
		this.testCase = testCase;
	}

	public List<String> getEnvs() {
		return envs;
	}

	public void setEnvs(List<String> envs) {
		this.envs = envs;
	}

	public String getDriverEnv() {
		return driverEnv;
	}

	public void setDriverEnv(String driverEnv) {
		this.driverEnv = driverEnv;
	}

	@Override
	public String toString() {
		return "TestConfig [testCase=" + testCase + ", envs=" + envs + ", driverEnv=" + driverEnv + "]";
	}
}