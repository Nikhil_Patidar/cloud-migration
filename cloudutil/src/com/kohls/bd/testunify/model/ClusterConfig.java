package com.kohls.bd.testunify.model;

import org.springframework.stereotype.Component;

@Component
public class ClusterConfig {

	
	private String azkabanURL;
	private String azkabanPort;
	private String password;
	private String jobHistoryServerURL;
	private String jobHistoryServerPort;
	private String userName;
	private String hiveMetastoreURL;
	private String hiveMetastorePort;

	public String getAzkabanURL() {
		return azkabanURL;
	}

	public void setAzkabanURL(String azkabanURL) {
		this.azkabanURL = azkabanURL;
	}

	public String getAzkabanPort() {
		return azkabanPort;
	}

	public void setAzkabanPort(String azkabanPort) {
		this.azkabanPort = azkabanPort;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJobHistoryServerURL() {
		return jobHistoryServerURL;
	}

	public void setJobHistoryServerURL(String jobHistoryServerURL) {
		this.jobHistoryServerURL = jobHistoryServerURL;
	}

	public String getJobHistoryServerPort() {
		return jobHistoryServerPort;
	}

	public void setJobHistoryServerPort(String jobHistoryServerPort) {
		this.jobHistoryServerPort = jobHistoryServerPort;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getHiveMetastoreURL() {
		return hiveMetastoreURL;
	}

	public void setHiveMetastoreURL(String hiveMetastoreURL) {
		this.hiveMetastoreURL = hiveMetastoreURL;
	}

	public String getHiveMetastorePort() {
		return hiveMetastorePort;
	}

	public void setHiveMetastorePort(String hiveMetastorePort) {
		this.hiveMetastorePort = hiveMetastorePort;
	}

}