package com.kohls.bd.testunify.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.TestConfig;
import com.kohls.bd.testunify.model.TestOutput;

@Component
public interface TestUnifyService {
	public List<TestOutput> executeTests(List<TestConfig> testConfigs);

}
