package com.kohls.bd.testunify.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.ClusterConfig;
@Component
public class ClusterConfigRepositoryImpl implements ClusterConfigRepository {

	Map<String, ClusterConfig> configMap = new HashMap<>();

	@Override
	public ClusterConfig getConfig(String env) {
		return configMap.get(env);
	}

	@Override
	public void setConfig(String env, ClusterConfig config) {
		configMap.put(env, config);
	}

	@Override
	public Map<String, ClusterConfig> getAllConfig() {
		return configMap;
	}

}
