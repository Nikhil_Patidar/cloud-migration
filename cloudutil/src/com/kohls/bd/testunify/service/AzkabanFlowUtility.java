package com.kohls.bd.testunify.service;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface AzkabanFlowUtility {

	List<String> getOutputTables(String projectName, String flowName, String job, int execId);

}
