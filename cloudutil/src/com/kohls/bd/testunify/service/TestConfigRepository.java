package com.kohls.bd.testunify.service;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.TestConfig;

@Component
public interface TestConfigRepository {

	TestConfig getTestConfig(String testCase);

	void setTestConfig(String testCase, TestConfig config);

	Map<String, TestConfig> getAllTestConfig();
}
