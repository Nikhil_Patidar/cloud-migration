package com.kohls.bd.testunify.service;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.TestCase;

@Component
public interface TestRepository {

	public TestCase getTestCase(String testCase);
	
	public void setTestCase(String name, TestCase testCase );

	Map<String,TestCase> getAllTestCase();

}
