package com.kohls.bd.testunify.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.jboss.netty.util.internal.ConcurrentHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.datametica.azkabanutil.AzkabanService;
import com.datametica.azkabanutil.model.ExecuteFlowOutput;
import com.datametica.azkabanutil.model.FlowGraph;
import com.kohls.bd.testunify.model.ClusterConfig;
import com.kohls.bd.testunify.model.TestCase;
import com.kohls.bd.testunify.model.TestConfig;
import com.kohls.bd.testunify.model.TestOutput;

@Component
public class TestUnifyServiceImpl implements TestUnifyService {

	private final Logger logger = LoggerFactory.getLogger(TestUnifyServiceImpl.class);

	@Autowired
	private ReconService reconService;

	@Autowired
	private ReAirService reairService;

	//@Autowired
	//private AzkabanFlowUtility azkabanFlowUtility;

	@Autowired
	private TestRepository testRepository;

	@Autowired
	ClusterConfig clusterConfig;
	
	@Autowired
	private ClusterConfigRepository clusterConfigRepository;
	
	private ExecutorService executorService = Executors.newCachedThreadPool();

	@Override
	public List<TestOutput> executeTests(List<TestConfig> testConfigs) {
	testConfigs.forEach(t->{
	/*	System.out.println("Current Driver Environment : "+t.getDriverEnv());
		System.out.println("Test Case : "+ t.getTestCase());
		System.out.println("Environments  : "+ t.getEnvs().toString());*/
	});
		testConfigs.forEach(t -> {
			System.out.println("********** Driver : "+t.getDriverEnv());	
			
			TestCase testCase = testRepository.getTestCase(t.getTestCase());
			clusterConfig = clusterConfigRepository.getConfig(t.getDriverEnv());
			AzkabanService driverAzkabanService =  getAzkabanService(clusterConfig);
			FlowGraph jobs = null;
			List<String> listJobs = new ArrayList<>();
			try {
				jobs = driverAzkabanService.getJobs(testCase.getProjectName(), testCase.getFlowName());
				jobs.getNodes().forEach((node) -> {
					 System.out.println(node.getId());
					 listJobs.add(node.getId());
					 });
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			// Shoud be populate with flowOutput.getExecId;

			//int execId = 0;

			Map<String, Integer> execMap = new ConcurrentHashMap<>();


			// in loop for flow
			List<Future<String>> futurs = new ArrayList<>();
 			t.getEnvs().forEach(env -> {
				futurs.add(executorService.submit(() -> {
					AzkabanService azkabanService = getAzkabanService(clusterConfigRepository.getConfig(env));
					Set<String> skipJobs = new HashSet<>(testCase.getSkipJobs());

					//System.out.println("Skip Jobs"+skipJobs.toString());
					if (env != t.getDriverEnv()) {
						skipJobs.addAll(testCase.getSkipJobsWithDataCopy());
					}
					ExecuteFlowOutput flowOutput = azkabanService.runJobs(testCase.getProjectName(), testCase.getFlowName(),new HashSet<>());
					System.out.println("*********   "+flowOutput.getExecid() + "  ***************");
					logger.debug("*********   "+flowOutput.toString()+ "  ***************");
					String status = azkabanService.waitTillFlowComplete(testCase.getProjectName(), testCase.getFlowName(),flowOutput.getExecid(),env);
					System.out.println("********* st : "+status);
					if (env.equals(t.getDriverEnv())) {
						execMap.put(testCase.getFlowName(), flowOutput.getExecid());
					}

					return status;
				}));
			});
 			
			/*
 			boolean finalStatus = true;
 			for(Future<String> future : futurs) {
 				try {
 					System.out.println(future.get());
					if(!future.get().equals("SUCCESS")) {
						finalStatus = false;
					}
				} catch (Exception e) {
					e.printStackTrace();
					finalStatus = false;
				}
 			}
 			if(!finalStatus) {
 				//test is failed. return from here,
 			}*/
 			

			/*testCase.getSkipJobsWithDataCopy().forEach(job -> {
				List<String> outputTables = azkabanFlowUtility.getOutputTables(testCase.getProjectName(),
						testCase.getFlowName(), job, execId);
				reairService.dataCopyHiveTable(outputTables, t.getDriverEnv(), t.getEnvs());
			});*/

			/*testCase.getReconJobs().forEach(job -> {
				reconService.compareOutputOnAllEnv(execId, job, t.getDriverEnv(), t.getEnvs());

			});*/


		});

		return null;
	}


	// TODO:Viren should be moved to config and @Bean scope
	private AzkabanService getAzkabanService(ClusterConfig env) {
		return 	new AzkabanService(clusterConfig.getAzkabanURL()+":"+clusterConfig.getAzkabanPort()+"/",clusterConfig.getUserName(),clusterConfig.getPassword());
	}

}
