package com.kohls.bd.testunify.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class ReAirServiceImpl implements ReAirService {
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public void dataCopyHiveTable(List<String> outputTables, String driverEnv, List<String> envs) {

		String reAirURL = "http://10.206.64.172:8181/shell/reAir" + "?TableListPath=/tmp/reairinput.txt";
		

		

		ReAirInput input = new ReAirInput();
		input.setBatchOutputDir("gs://kohls-dev-infra-staging/tkmah7u/adityadb1.db/avro_poc4_output");
		input.setDestHdfsRoot("gs://kohls-dev-infra-staging/");
		input.setDestHdfsTmp("gs://kohls-dev-infra-staging/tkmah7u/adityadb1.db/avro_poc4_src_tmp");
		input.setDestMetastoreUrl("thrift://ddh-dev-dataproc-m-0:9083");
		input.setDestName("GOOGLE CLOUD");
		input.setSrcHdfsRoot("hdfs://KOHLSBIGDATA2NNHA/");
		input.setSrcHdfsTmp("gs://kohls-dev-infra-staging/tkmah7u/adityadb1.db/avro_poc4_src_tmp");
		input.setSrcMetastoreUrl("thrift://10.200.0.66:9083");
		input.setSrcName("DDH CLUSTER");
		
		ResponseEntity<String> output = restTemplate.postForEntity(reAirURL, input, String.class);
		System.out.println(output.getBody());
	}

	public static void main(String[] args) {
		System.out.println("vire");
		ReAirService airService = new ReAirServiceImpl();

		airService.dataCopyHiveTable(null, null, null);
		
	}

	/*
	 * private <T> String getDataFromRest(String reAirURL)
	 * //MultiValueMap<String, String> parameters) throws RestClientException {
	 * UriComponentsBuilder urlBuilder =
	 * UriComponentsBuilder.fromHttpUrl(reAirURL);
	 * //urlBuilder.queryParams(parameters); String url =
	 * urlBuilder.build().encode().toUriString(); String response =
	 * restTemplate.postForObject(reAirURL, request, responseType,
	 * uriVariables)(url, String.class); return response; }
	 */

	// TODO Auto-generated method stub

}
