package com.kohls.bd.testunify.service;


import java.util.HashMap;
import java.util.Map;

//@JsonInclude(JsonInclude.Include.NON_NULL)
/*//@JsonPropertyOrder({
"srcName",
"srcMetastoreUrl",
"srcHdfsRoot",
"srcHdfsTmp",
"destName",
"destMetastoreUrl",
"destHdfsRoot",
"destHdfsTmp",
"batchOutputDir"
})*/
public class ReAirInput {

	//// @JsonProperty("srcName")
	private String srcName;
	//// @JsonProperty("srcMetastoreUrl")
	private String srcMetastoreUrl;
	//// @JsonProperty("srcHdfsRoot")
	private String srcHdfsRoot;
	//// @JsonProperty("srcHdfsTmp")
	private String srcHdfsTmp;
	//// @JsonProperty("destName")
	private String destName;
	//// @JsonProperty("destMetastoreUrl")
	private String destMetastoreUrl;
	//// @JsonProperty("destHdfsRoot")
	private String destHdfsRoot;
	//// @JsonProperty("destHdfsTmp")
	private String destHdfsTmp;
	//// @JsonProperty("batchOutputDir")
	private String batchOutputDir;
	//// @JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	//// @JsonProperty("srcName")
	public String getSrcName() {
		return srcName;
	}

	// @JsonProperty("srcName")
	public void setSrcName(String srcName) {
		this.srcName = srcName;
	}

	// @JsonProperty("srcMetastoreUrl")
	public String getSrcMetastoreUrl() {
		return srcMetastoreUrl;
	}

	// @JsonProperty("srcMetastoreUrl")
	public void setSrcMetastoreUrl(String srcMetastoreUrl) {
		this.srcMetastoreUrl = srcMetastoreUrl;
	}

	// @JsonProperty("srcHdfsRoot")
	public String getSrcHdfsRoot() {
		return srcHdfsRoot;
	}

	// @JsonProperty("srcHdfsRoot")
	public void setSrcHdfsRoot(String srcHdfsRoot) {
		this.srcHdfsRoot = srcHdfsRoot;
	}

	// @JsonProperty("srcHdfsTmp")
	public String getSrcHdfsTmp() {
		return srcHdfsTmp;
	}

	// @JsonProperty("srcHdfsTmp")
	public void setSrcHdfsTmp(String srcHdfsTmp) {
		this.srcHdfsTmp = srcHdfsTmp;
	}

	// @JsonProperty("destName")
	public String getDestName() {
		return destName;
	}

	// @JsonProperty("destName")
	public void setDestName(String destName) {
		this.destName = destName;
	}

	// @JsonProperty("destMetastoreUrl")
	public String getDestMetastoreUrl() {
		return destMetastoreUrl;
	}

	// @JsonProperty("destMetastoreUrl")
	public void setDestMetastoreUrl(String destMetastoreUrl) {
		this.destMetastoreUrl = destMetastoreUrl;
	}

	// @JsonProperty("destHdfsRoot")
	public String getDestHdfsRoot() {
		return destHdfsRoot;
	}

	// @JsonProperty("destHdfsRoot")
	public void setDestHdfsRoot(String destHdfsRoot) {
		this.destHdfsRoot = destHdfsRoot;
	}

	// @JsonProperty("destHdfsTmp")
	public String getDestHdfsTmp() {
		return destHdfsTmp;
	}

	// @JsonProperty("destHdfsTmp")
	public void setDestHdfsTmp(String destHdfsTmp) {
		this.destHdfsTmp = destHdfsTmp;
	}

	// @JsonProperty("batchOutputDir")
	public String getBatchOutputDir() {
		return batchOutputDir;
	}

	// @JsonProperty("batchOutputDir")
	public void setBatchOutputDir(String batchOutputDir) {
		this.batchOutputDir = batchOutputDir;
	}

	// @JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	// @JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}