package com.kohls.bd.testunify.service;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.ClusterConfig;

@Component
public interface ClusterConfigRepository {

	ClusterConfig getConfig(String config);

	void setConfig(String env, ClusterConfig config);

	Map<String, ClusterConfig> getAllConfig();

}
