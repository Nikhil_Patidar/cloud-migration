package com.kohls.bd.testunify.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.TestCase;

@Component
public class TestRepositoryImpl implements TestRepository {

	private Map<String, TestCase> testCases = new HashMap<>();

	@Override
	public TestCase getTestCase(String testCase) {
		return testCases.get(testCase);
	}

	@Override
	public Map<String, TestCase> getAllTestCase (){
		return this.testCases;
	}
	@Override
	public void setTestCase(String name, TestCase testCase) {
		testCases.put(name, testCase);
	}
}
