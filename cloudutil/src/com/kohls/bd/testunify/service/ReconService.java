package com.kohls.bd.testunify.service;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface ReconService {

	void compareOutputOnAllEnv(int execId, String job, String driverEnv, List<String> envs);

}
