package com.kohls.bd.testunify.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.datametica.unifycommon.model.ClusterConfig;

@Component
public class ClusterConfigRepositoryImpl implements ClusterConfigRepository {

	Map<String, ClusterConfig> configMap = new HashMap<>();

	@PostConstruct
	public void init() {
		// load all files from folder
	}

	@Override
	public ClusterConfig getConfig(String env) {
		return configMap.get(env);
	}

	@Override
	public void setConfig(String env, ClusterConfig config) {
		configMap.put(env, config);
		// also put this data in file and put in folder
	}

	@Override
	public Map<String, ClusterConfig> getAllConfig() {
		return configMap;
	}

}
