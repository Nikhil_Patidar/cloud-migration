package com.kohls.bd.testunify.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.LineageInput;
import com.datametica.unifycommon.model.LineageOutput;

@Component
public class AzkabanFlowUtilityImpl implements AzkabanFlowUtility {
	private final Logger logger = LoggerFactory.getLogger(AzkabanFlowUtilityImpl.class);

	private RestTemplate restTemplate = new RestTemplate();
	// Hard Coded URL
	String azkabanIOUtilityURL = "http://10.206.52.20:8765/lineage-service/";

	@Override
	public LineageOutput getOutputTables(LineageInput lineageInput) {
		String azkabanInputOutputUtilityURL = azkabanIOUtilityURL + "lineage-job";
		StringBuilder urlToRest = new StringBuilder(azkabanInputOutputUtilityURL).append("?env=")
				.append(lineageInput.getEnv()).append("&flowName=").append(lineageInput.getFlowName())
				.append("&projectName=").append(lineageInput.getProjectName()).append("&jobId=")
				.append(lineageInput.getJobId()).append("&executionId=").append(lineageInput.getExecutionId());

		logger.info("Rest URL for Azkaban I/O Utility  : " + urlToRest.toString());
		ResponseEntity<LineageOutput> output = restTemplate.getForEntity(urlToRest.toString(), LineageOutput.class);
		logger.debug("Output for lineage service to get output tables" + output.getBody().toString());
		return output.getBody();
	}

	@Override
	public HttpStatus setAzkabanConfig(ClusterConfig clusterConfig) {
		logger.info("Trying to set config for Azkaban Flow Utility");
		ResponseEntity<String> output = restTemplate.postForEntity(azkabanIOUtilityURL + "properties", clusterConfig,
				String.class);
		logger.debug("output for setting config " + output.getBody());
		return output.getStatusCode();
	}

}
