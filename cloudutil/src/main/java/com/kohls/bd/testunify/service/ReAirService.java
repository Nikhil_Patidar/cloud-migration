package com.kohls.bd.testunify.service;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.LineageOutput;

@Component
public interface ReAirService {

	HttpStatus setConfig(ClusterConfig reAirHiveConfig);

	String executeDataCopy(LineageOutput outputTables, String driverEnv, List<String> envs);
}
