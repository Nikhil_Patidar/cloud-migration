package com.kohls.bd.testunify.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.datametica.azkabanutil.exception.ExternalServiceException;
import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.DistcpUtilityInput;
import com.datametica.unifycommon.model.LineageOutput;
import com.datametica.unifycommon.model.ReAirInput;

@Component
public class ReAirServiceImpl implements ReAirService {
	private static final String HTTP_SHELL = "http://10.206.52.20:8766/shell/";
	private static final String REAIR_URL = HTTP_SHELL + "reAir/execute";
	private RestTemplate restTemplate = new RestTemplate();
	private static final String CONFIG_URL = HTTP_SHELL + "clusterConfig";
	private final Logger logger = LoggerFactory.getLogger(ReAirServiceImpl.class);

	@Override
	public HttpStatus setConfig(ClusterConfig config) {
		logger.info("Trying to set config for Shell Service for further use.");
		ResponseEntity<Void> output = restTemplate.postForEntity(CONFIG_URL, config, Void.class);
		if (output.getStatusCode() == HttpStatus.UNPROCESSABLE_ENTITY) {
			logger.error("Cannot store cluster config due to issues ");
			throw new ExternalServiceException("Error occurred while saving cluster config");
		} else if (output.getStatusCode() == HttpStatus.CREATED) {
			logger.info("created new cluster config at shell service.");
			return output.getStatusCode();
		} else {
			logger.info("There was entity already there which has been updated.");
			return output.getStatusCode();
		}
	}

	@Override
	public String executeDataCopy(LineageOutput lineageOutput, String driverEnv, List<String> envs) {
		logger.info("Trying to execute data copy using reAir Service");

		/*
		 * List<String> outputTables = new ArrayList<>();
		 * lineageOutput.getOutputDetail().forEach(t ->
		 * outputTables.add(t.getDbName() + "." + t.getTableName()));
		 * 
		 * envs.forEach(t -> { if(!t.equalsIgnoreCase(driverEnv)){ ReAirInput
		 * reairInput = new ReAirInput();
		 * reairInput.setOutputTables(outputTables);
		 * reairInput.setSrcEnv(driverEnv); reairInput.setDestEnv(t);
		 * ResponseEntity<String> output = restTemplate.postForEntity(REAIR_URL,
		 * reairInput, String.class); logger.info("output : " + output); } } );
		 */
		List<String> outputTables = new ArrayList<>();
		List<String> outputLocations = new ArrayList<>();
		// List<TableDetail> tableDetails = lineageOutput.getOutputDetail();

		lineageOutput.getOutputDetail().forEach(t -> {
			if (t.getDbName() != null && t.getTableName() != null)
				outputTables.add(t.getDbName() + "." + t.getTableName());
			else if (t.getDbName() == null && t.getTableName() == null && t.getFileDir() != null)
				outputLocations.add(t.getFileDir());
		});

		envs.forEach(t -> {
			logger.info("Executing ReAir for copyting tables from "+driverEnv+" to "+t);
			if (!t.equalsIgnoreCase(driverEnv) && outputTables != null) {
				ReAirInput reairInput = new ReAirInput();
				reairInput.setOutputTables(outputTables);
				reairInput.setSrcEnv(driverEnv);
				reairInput.setDestEnv(t);
				ResponseEntity<String> output = restTemplate.postForEntity(REAIR_URL, reairInput, String.class);
				logger.info("output : " + output);
			}
		});

		envs.forEach(t -> {
			logger.info("Executing Distcp for copying data from "+driverEnv+" to "+t);
			if (!t.equalsIgnoreCase(driverEnv) && outputLocations != null) {
				DistcpUtilityInput distcpInput = new DistcpUtilityInput();
				distcpInput.setListOfPath(outputLocations);
				distcpInput.setSrcEnv(driverEnv);
				distcpInput.setDestEnv(t);
				ResponseEntity<String> output = restTemplate.postForEntity(REAIR_URL, distcpInput, String.class);
				logger.info("output : " + output);
			}
		});
		logger.info("Data Copy done");
		return "All Data Copy done for output Tables : " + outputTables + "from " + driverEnv + " to " + envs;

		/*
		 * input.setBatchOutputDir(
		 * "gs://kohls-dev-infra-staging/tkmah7u/adityadb1.db/avro_poc4_output")
		 * ; input.setDestHdfsRoot("gs://kohls-dev-infra-staging/");
		 * input.setDestHdfsTmp(
		 * "gs://kohls-dev-infra-staging/tkmah7u/adityadb1.db/avro_poc4_src_tmp"
		 * ); input.setDestMetastoreUrl("thrift://ddh-dev-dataproc-m-0:9083");
		 * input.setDestName("GOOGLE CLOUD");
		 * input.setSrcHdfsRoot("hdfs://KOHLSBIGDATA2NNHA/");
		 * input.setSrcHdfsTmp(
		 * "gs://kohls-dev-infra-staging/tkmah7u/adityadb1.db/avro_poc4_src_tmp"
		 * ); input.setSrcMetastoreUrl("thrift://10.200.0.66:9083");
		 * input.setSrcName("DDH CLUSTER");
		 */

	}
}
