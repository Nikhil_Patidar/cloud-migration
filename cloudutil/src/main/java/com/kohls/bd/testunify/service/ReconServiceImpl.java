package com.kohls.bd.testunify.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.LineageOutput;
import com.datametica.unifycommon.model.ReconInput;
import com.datametica.unifycommon.model.TableDTO;

@Component
public class ReconServiceImpl implements ReconService {

	private final Logger logger = LoggerFactory.getLogger(ReconServiceImpl.class);

	private static String HTTP_SHELL = "http://10.206.52.20:8576";
	private static String RECON_URL = HTTP_SHELL + "/recon-service/recon ";
	private static final String CONFIG_URL = HTTP_SHELL + "/recon-service/config";
	private RestTemplate restTemplate = new RestTemplate();

	@Override
	public HttpStatus setReconConfig(ClusterConfig clusterConfig) {
		logger.info("Trying to set config for Recon Utility");
		ResponseEntity<String> output = restTemplate.postForEntity(CONFIG_URL, clusterConfig, String.class);
		return output.getStatusCode();
	}

	@Override
	public List<String> compareOutputTables(LineageOutput lineageOutput, List<String> envs, String filterConditions) {
		List<String> outputTables = new ArrayList<>();
		List<String> reconOutputs = new ArrayList<>();

		lineageOutput.getOutputDetail().forEach(t -> {
			if (t.getDbName() != null && t.getTableName() != null)
				outputTables.add(t.getDbName() + "." + t.getTableName());
		});

		for (String table : outputTables) {
			ReconInput reconInput = new ReconInput();
			List<TableDTO> tableDetailsList = new ArrayList<>();
			for (String env : envs) {
				TableDTO tableDTO = new TableDTO();
				tableDTO.setDbName(table.substring(0, table.indexOf(".")));
				tableDTO.setTblName(table.substring(table.indexOf(".") + 1));
				tableDTO.setFilterCondition(filterConditions);
				tableDTO.setEnv(env);
				tableDetailsList.add(tableDTO);
			}
			reconInput.setTableDetailsList(tableDetailsList);
			// Object uriVariables = reconInput;
			ResponseEntity<String> output = restTemplate.postForEntity(RECON_URL, reconInput, String.class);
			reconOutputs.add(output.getBody());
		}
		return reconOutputs;
	}
}