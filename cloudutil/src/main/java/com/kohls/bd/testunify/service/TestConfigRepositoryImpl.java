package com.kohls.bd.testunify.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.kohls.bd.testunify.model.TestConfig;

@Component
public class TestConfigRepositoryImpl implements TestConfigRepository{

	Map<String, TestConfig> configMap = new HashMap<>();


	@Override
	public TestConfig getTestConfig(String testCase) {
		return configMap.get(testCase);
	}

	@Override
	public void setTestConfig(String testCase, TestConfig config) {	
		configMap.put(testCase, config);
	}

	@Override
	public Map<String, TestConfig> getAllTestConfig() {
		return this.configMap;
	}
	

}
