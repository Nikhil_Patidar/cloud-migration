package com.kohls.bd.testunify.service;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.LineageOutput;

@Component
public interface ReconService {

	// String compareOutputTables(int execId, String job, String driverEnv,
	// List<String> envs);

	public HttpStatus setReconConfig(ClusterConfig clusterConfig);

	List<String> compareOutputTables(LineageOutput outputTables, List<String> envs, String filterConditions);
}
