package com.kohls.bd.testunify.service;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.datametica.unifycommon.model.ClusterConfig;

@Component
public interface ClusterConfigRepository {

	ClusterConfig getConfig(String env);

	void setConfig(String env, ClusterConfig config);

	Map<String, ClusterConfig> getAllConfig();

}
