package com.kohls.bd.testunify.service;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datametica.unifycommon.model.ClusterConfig;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kohls.bd.testunify.model.TestCase;

public class SaveClusterConfig {
	
	private final Logger logger = LoggerFactory.getLogger(SaveClusterConfig.class);
	
	public void saveConfigInFile(String env, ClusterConfig clusterConfig) throws IOException {
		String fileName = "/tmp" + File.separator + env + ".json";
		ObjectMapper mapper = new ObjectMapper();
		mapper.writeValue(new File(fileName), clusterConfig);
	}

	/*public void saveConfigInFileFromInput(String fileName, Object testCase) throws IOException {
		logger.info("Saving file: "+fileName);
		ObjectMapper mapper = new ObjectMapper();
		mapper.readValue(new File(fileName), testCase);
		//mapper.readValue(new File(fileName), testCase);
		logger.info("Saved object: "+testCase.toString());
	}*/
	
	public ClusterConfig saveConfigFromFileInClusterConfig(String fileName) throws JsonParseException, JsonMappingException, IOException{
		logger.info("Saving file: "+fileName+" for cluster config");
		ObjectMapper mapper = new ObjectMapper();
		ClusterConfig clusterConfig = mapper.readValue(new File(fileName), ClusterConfig.class);
		logger.info("Saved Config: "+clusterConfig);
		return clusterConfig;
	}
	
	public TestCase saveConfigFromFileInTestCase(String fileName) throws JsonParseException, JsonMappingException, IOException{
		logger.info("Saving file: "+fileName+" for testcase");
		ObjectMapper mapper = new ObjectMapper();
		TestCase testCase = mapper.readValue(new File(fileName), TestCase.class);
		logger.info("Saved testcase: "+testCase);
		return testCase;
	}
}
