package com.kohls.bd.testunify.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class TestCase implements Serializable {

	private static final long serialVersionUID = 1L;

	private String driverEnvName;
	private String testCaseName;
	private String srcProjectName;
	private String srcFlowName;
	private String destProjectName;
	private String destFlowName;
	private List<String> reconJobs;
	private List<String> skipJobsWithDataCopy = null;
	private List<String> skipJobs = null;
	private List<String> checkInput = null;
	private List<InputLocation> inputLocation = null;
	private String filterConditions;

	public String getDriverEnvName() {
		return driverEnvName;
	}

	public void setDriverEnvName(String driverEnvName) {
		this.driverEnvName = driverEnvName;
	}

	public String getTestCaseName() {
		return testCaseName;
	}

	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}

	public String getSrcProjectName() {
		return srcProjectName;
	}

	public void setSrcProjectName(String srcProjectName) {
		this.srcProjectName = srcProjectName;
	}

	public String getSrcFlowName() {
		return srcFlowName;
	}

	public void setSrcFlowName(String srcFlowName) {
		this.srcFlowName = srcFlowName;
	}

	public String getDestProjectName() {
		return destProjectName;
	}

	public void setDestProjectName(String destProjectName) {
		this.destProjectName = destProjectName;
	}

	public String getDestFlowName() {
		return destFlowName;
	}

	public void setDestFlowName(String destFlowName) {
		this.destFlowName = destFlowName;
	}

	public List<String> getReconJobs() {
		return reconJobs;
	}

	public void setReconJobs(List<String> reconJobs) {
		this.reconJobs = reconJobs;
	}

	public List<String> getSkipJobsWithDataCopy() {
		return skipJobsWithDataCopy;
	}

	public void setSkipJobsWithDataCopy(List<String> skipJobsWithDataCopy) {
		this.skipJobsWithDataCopy = skipJobsWithDataCopy;
	}

	public List<String> getSkipJobs() {
		return skipJobs;
	}

	public void setSkipJobs(List<String> skipJobs) {
		this.skipJobs = skipJobs;
	}

	public List<String> getCheckInput() {
		return checkInput;
	}

	public void setCheckInput(List<String> checkInput) {
		this.checkInput = checkInput;
	}

	public List<InputLocation> getInputLocation() {
		return inputLocation;
	}

	public void setInputLocation(List<InputLocation> inputLocation) {
		this.inputLocation = inputLocation;
	}

	public String getFilterConditions() {
		return filterConditions;
	}

	public void setFilterConditions(String filterConditions) {
		this.filterConditions = filterConditions;
	}

}