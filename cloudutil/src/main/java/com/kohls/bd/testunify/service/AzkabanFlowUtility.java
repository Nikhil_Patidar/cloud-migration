package com.kohls.bd.testunify.service;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.LineageInput;
import com.datametica.unifycommon.model.LineageOutput;

@Component
public interface AzkabanFlowUtility {

	public LineageOutput getOutputTables(LineageInput lineageInput);

	public HttpStatus setAzkabanConfig(ClusterConfig clusterConfig);

}
