package com.kohls.bd.cloudmigration;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.datametica.azkabanutil.AzkabanService;
import com.datametica.azkabanutil.model.ExecuteFlowOutput;
import com.datametica.azkabanutil.model.FlowExecution;
import com.datametica.unifycommon.model.ClusterConfig;
import com.datametica.unifycommon.model.LineageInput;
import com.datametica.unifycommon.model.LineageOutput;
import com.datametica.unifycommon.model.ReconInput;
import com.datametica.unifycommon.model.TableDTO;
import com.kohls.bd.testunify.model.TestCase;
import com.kohls.bd.testunify.model.TestConfig;
import com.kohls.bd.testunify.service.AzkabanFlowUtility;
import com.kohls.bd.testunify.service.AzkabanFlowUtilityImpl;
import com.kohls.bd.testunify.service.ClusterConfigRepository;
import com.kohls.bd.testunify.service.ClusterConfigRepositoryImpl;
import com.kohls.bd.testunify.service.ReAirService;
import com.kohls.bd.testunify.service.ReAirServiceImpl;
import com.kohls.bd.testunify.service.ReconService;
import com.kohls.bd.testunify.service.ReconServiceImpl;
import com.kohls.bd.testunify.service.SaveClusterConfig;

public class CloudMigration {

	@Autowired
	static ClusterConfigRepository clusterConfigRepository = new ClusterConfigRepositoryImpl();

	@Autowired
	static AzkabanFlowUtility azkabanFlowUtility = new AzkabanFlowUtilityImpl();

	@Autowired
	static ReconService reconService = new ReconServiceImpl();

	@Autowired
	static ReAirService reairService = new ReAirServiceImpl();

	private static String HTTP_SHELL = "http://10.206.52.20:8576";
	private static String RECON_URL = HTTP_SHELL + "/recon-service/recon ";
	private RestTemplate restTemplate = new RestTemplate();
	private static Logger log = Logger.getLogger(CloudMigration.class);

	private static Integer srcExid;
	private static Integer destExid;
	private static String status;
	private static long starttime;
	private static String clusterConfigLoc;
	private static String testCaseFile;
	private static SaveClusterConfig saveConfigFromFile;
	private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	private static TestConfig testConf;
	private static TestCase testCase;
	private static Map<String, Integer> execMap = new ConcurrentHashMap<>();
	private static Set<String> dataCopyJobsAtParticularDepth;
	private static ClusterConfig clusterConfig;

	CloudMigration(String[] args) {
		clusterConfig = new ClusterConfig();
		log.info(new Date().toString() + " CloudMigration Testing Started !!");

		log.info("Setting Cluster configurations and Test case");

		clusterConfigLoc = args[0];
		testCaseFile = args[1];

		saveConfigFromFile = new SaveClusterConfig();
		try {
			File[] files = new File(clusterConfigLoc + File.separator).listFiles();
			for (File file : files) {
				try {
					clusterConfig = saveConfigFromFile
							.saveConfigFromFileInClusterConfig(clusterConfigLoc + File.separator + file.getName());
				} catch (Exception e) {
					log.error("Error in creating Cluster Config: " + e);
					System.exit(0);
				}
				if (clusterConfig == null) {
					log.error("ClusterConfig is NULL !!");
					System.exit(0);
				}
				log.info("ClusterConfig: " + clusterConfig);
				clusterConfigRepository.setConfig(clusterConfig.getEnv(), clusterConfig);

				HttpStatus azkabanStatus = azkabanFlowUtility.setAzkabanConfig(clusterConfig);
				if (azkabanStatus == HttpStatus.OK)
					log.info("Azkaban Configuration - SUCCESS");
				else
					log.error("Azkaban Configuration - FAIL");
				HttpStatus reairStatus = reairService.setConfig(clusterConfig);
				if (reairStatus == HttpStatus.OK)
					log.info("ReAir Configuration - SUCCESS");
				else
					log.error("ReAir Configuration - FAIL");
				HttpStatus reconStatus = reconService.setReconConfig(clusterConfig);
				if (reconStatus == HttpStatus.OK)
					log.info("Recon Configuration - SUCCESS");
				else
					log.error("Recon Configuration - FAIL");

			}
			testCase = new TestCase();
			testCase = saveConfigFromFile.saveConfigFromFileInTestCase(testCaseFile);
		} catch (IOException e1) {
			log.error("Unable to Save the Config");
			e1.printStackTrace();
		}
		log.info("Setting all the configurations !!");
		setTestConfig();
	}

	public static void main(String[] args) {
		log.info("Confiiguratin file: " + args[0]);
		log.info("Testcase file: " + args[1]);
		CloudMigration cloudMigration = new CloudMigration(args);
		try {
			if (cloudMigration.execution()) {
				System.exit(0);
			} else {
				log.info("process failed");
				System.exit(1);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private boolean execution() throws InterruptedException {
		log.info("Fetching Azkaban Details from " + testConf.getDriverEnv() + " for Project: "
				+ testCase.getSrcProjectName() + " Flow: " + testCase.getSrcFlowName());
		ClusterConfig clusterConfig = clusterConfigRepository.getConfig(testConf.getDriverEnv());

		if (!azkabanDetails(clusterConfig.getAzkabanUrl(), clusterConfig.getAzkabanUserName(),
				clusterConfig.getAzkabanPassword(), testCase.getSrcProjectName(), testCase.getSrcFlowName()))
			System.exit(0);

		log.info("Azkaban flow's submit date: " + new Date(starttime).toString() + " and today's date: "
				+ new Date().toString());
		if (dateFormat.format(new Date()).equalsIgnoreCase(dateFormat.format(new Date(starttime)))) {
			log.info("Azkaban flow status: " + status + " for env: " + testCase.getDriverEnvName());
			if (status.equalsIgnoreCase("SUCCEEDED")) {
				log.info("Azkaban flow status matched for " + testCase.getDriverEnvName());
				execMap.put(testCase.getSrcFlowName(), srcExid);

				log.info("Checking for any skip jobs with data copy");
				if (testCase.getSkipJobsWithDataCopy() != null && testCase.getSkipJobsWithDataCopy().size() > 1
						&& !testCase.getSkipJobsWithDataCopy().isEmpty()) {
					log.info("User provided jobs to be skipped: " + testCase.getSkipJobsWithDataCopy().toString());
					dataCopyJobsAtParticularDepth.addAll(testCase.getSkipJobsWithDataCopy());
					log.info("Calling ReAir/Distcp to copy data from " + testCase.getDriverEnvName());
					executeReAir(testConf, testCase, execMap, dataCopyJobsAtParticularDepth);
				} else {
					log.info("No jobs to be skiped");
				}

				log.info("Executing Flow on " + testConf.getEnvs().get(0));
				destExid = executeJobsAtAzkaban(testCase, testConf.getEnvs().get(0), dataCopyJobsAtParticularDepth);

				if (destExid != null && destExid > 0) {
					log.info("Calling ReCon to compare jobs " + testCase.getReconJobs().toString() + " between "
							+ testCase.getDriverEnvName() + " and " + testConf.getEnvs().toString());
					executeRecon(testConf, testCase, execMap);
				}
				return true;
			} else {
				log.info("Current Time: " + new Date().toString()
						+ " - Azkaban flow status mismatch waiting for 5 mins");
				TimeUnit.MINUTES.sleep(5);
				execution();
			}
		} else {
			if (new Date().compareTo(new Date(starttime)) == 2) {
				log.info("Azkaban flow " + testCase.getDestFlowName() + " did not ran on " + new Date().toString());
				return false;
			} else {
				log.info("Current Time: " + new Date().toString()
						+ "- Azkaban flow did not ran till now waiting for 5 mins");
				TimeUnit.MINUTES.sleep(5);
				execution();
			}
		}
		return false;
	}

	public void executeReAir(TestConfig t, TestCase testCase, Map<String, Integer> execMap,
			Set<String> dataCopyJobsAtParticularDepth) {
		if (testCase.getSkipJobsWithDataCopy() == null) {
			return;
		}
		dataCopyJobsAtParticularDepth.forEach(dataCopyJob -> {
			log.info("Feteching the Flow Details from " + testCase.getDriverEnvName()
					+ " by REST Call to Azkaban IO utility");
			LineageOutput outputTables = azkabanFlowUtility
					.getOutputTables(prepareLineageInput(t.getDriverEnv(), testCase.getSrcFlowName(),
							testCase.getSrcProjectName(), dataCopyJob, execMap.get(testCase.getSrcFlowName())));
			if (outputTables.getJobName() != null)
				reairService.executeDataCopy(outputTables, t.getDriverEnv(), t.getEnvs());
			else
				log.warn("No output tables found at driver env : " + t.getDriverEnv() + " to ReAir for flow : "
						+ testCase.getSrcFlowName() + ". Execution Id : " + execMap.get(testCase.getSrcFlowName()));
		});
	}

	public void executeRecon(TestConfig t, TestCase testCase, Map<String, Integer> execMap) {
		if (testCase.getReconJobs() == null) {
			return;
		}
		List<String> envs = new ArrayList<>();
		envs.addAll(t.getEnvs());
		envs.add(t.getDriverEnv());
		testCase.getReconJobs().forEach(job -> {
			LineageOutput outputTables = azkabanFlowUtility
					.getOutputTables(prepareLineageInput(t.getDriverEnv(), testCase.getSrcFlowName(),
							testCase.getSrcProjectName(), job, execMap.get(testCase.getSrcFlowName())));
			reconService.compareOutputTables(outputTables, envs, testCase.getFilterConditions());
		});
	}

	public List<String> compareOutputTables(LineageOutput lineageOutput, List<String> envs, String filterConditions) {
		List<String> outputTables = new ArrayList<>();
		List<String> reconOutputs = new ArrayList<>();

		lineageOutput.getOutputDetail().forEach(t -> {
			if (t.getDbName() != null && t.getTableName() != null)
				outputTables.add(t.getDbName() + "." + t.getTableName());
		});

		for (String table : outputTables) {
			ReconInput reconInput = new ReconInput();
			List<TableDTO> tableDetailsList = new ArrayList<>();
			for (String env : envs) {
				TableDTO tableDTO = new TableDTO();
				tableDTO.setDbName(table.substring(0, table.indexOf(".")));
				tableDTO.setTblName(table.substring(table.indexOf(".") + 1));
				tableDTO.setFilterCondition(filterConditions);
				tableDTO.setEnv(env);
				tableDetailsList.add(tableDTO);
			}
			reconInput.setTableDetailsList(tableDetailsList);
			ResponseEntity<String> output = restTemplate.postForEntity(RECON_URL, reconInput, String.class);
			reconOutputs.add(output.getBody());
		}
		return reconOutputs;
	}

	private LineageInput prepareLineageInput(String env, String flowName, String projectName,
			String skipWithDataCopyJob, Integer execid) {
		LineageInput lineageInput = new LineageInput();
		lineageInput.setEnv(env);
		lineageInput.setExecutionId(execid);
		lineageInput.setFlowName(flowName);
		lineageInput.setJobId(skipWithDataCopyJob);
		lineageInput.setProjectName(projectName);
		return lineageInput;
	}

	public Integer executeJobsAtAzkaban(TestCase testCase, String env, Set<String> skipJobs) {
		try {
			
			AzkabanService azkabanService = azkabanService(env);
			ExecuteFlowOutput flowOutput = azkabanService.runJobs(testCase.getDestProjectName(),
					testCase.getDestFlowName(), skipJobs);
			log.debug("Run Job output is " + flowOutput);
			if (flowOutput.getMessage() == null) {
				throw new Exception("Error occurred while submitting job. Error : " + flowOutput.getError());
			}
			String status = azkabanService.waitTillFlowComplete(testCase.getDestProjectName(),
					testCase.getDestFlowName(), flowOutput.getExecid());
			if (status.equalsIgnoreCase("SUCCEEDED"))
				return flowOutput.getExecid();
			else
				return 0;
		} catch (Exception e) {
			throw new IllegalStateException("Error occurred while executing flow.", e);
		}
	}

	public AzkabanService azkabanService(String env) {
		log.info("Trying to create azkaban service for env : " + env);
		ClusterConfig clusterConfig = clusterConfigRepository.getConfig(env);
		try {
			return new AzkabanService(env, clusterConfig.getAzkabanUrl(), clusterConfig.getAzkabanUserName(),
					clusterConfig.getAzkabanPassword());
		} catch (Exception e) {
			log.error("Error occurred while creating azkaban service for given cluster config : " + clusterConfig, e);
			throw new IllegalStateException(
					"Cannot proceed further without creating azkaban service. It seem with provided cluster config, we cannot create azkaban service");
		}
	}

	private Boolean azkabanDetails(String url, String user, String pass, String projectName, String flowName) {
		log.info("Fetching Azkaban Details for URL:" + url + " user:" + user + " password:" + pass + " projectName:"
				+ projectName + " flowName:" + flowName);
		AzkabanService azkabanService = new AzkabanService(url, user, pass);
		FlowExecution flowExecution = azkabanService.getFlowExecutionStatus(projectName, flowName);
		if (flowExecution != null) {
			srcExid = flowExecution.getExecutions().get(0).getExecId();
			status = flowExecution.getExecutions().get(0).getStatus();
			starttime = flowExecution.getExecutions().get(0).getSubmitTime();
			log.info("Azkaban Details Successfully Stored");
			return true;
		} else {
			log.error("Cannot extract the flow execution information");
			return false;
		}
	}

	private void setTestConfig() {
		testConf = new TestConfig();
		List<String> testEnv = new ArrayList<String>();
		Map<String, ClusterConfig> cConfigMap = clusterConfigRepository.getAllConfig();
		for (String env : cConfigMap.keySet())
			if (!env.equalsIgnoreCase(testCase.getDriverEnvName()))
				testEnv.add(env);
		testConf.setDriverEnv(testCase.getDriverEnvName());
		testConf.setEnvs(testEnv);
		testConf.setTestCase(testCase.getTestCaseName());
	}
}