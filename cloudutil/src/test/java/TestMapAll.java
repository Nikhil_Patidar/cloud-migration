import java.util.HashMap;
import java.util.Map;

public class TestMapAll {

	public static void main(String[] args) {
		Map<String,String> map1 = new HashMap<>();
		map1.put("a", "a");
		map1.put("b", "b");
		Map<String,String> map2 = new HashMap<>();
		map2.put("c", "a");
		map2.put("d", "b");
		map2.put("a", "baa");
		
		Map<String,String> map3 = new HashMap<>();
		map3.putAll(map1);
		map3.putAll(map2);
	
		System.out.println(map3);
	}
	
}
